package model

import (
	"github.com/globalsign/mgo/bson"
)

type Character struct {
	ID          bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Name        string        `json:"name" bson:"name"`
	WeightLimit float32       `json:"weightLimit" bson:"weightLimit"`
}

var ExampleCharacters = []Character{
	Character{
		bson.ObjectId(""), "John Goodfellow", 32.0,
	},
	Character{
		bson.ObjectId(""), "Alrik Immerda", 42.0,
	},
}
